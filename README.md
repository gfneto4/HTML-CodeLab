# HTML-CodeLab

<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Apresentação</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <link rel="stylesheet" href="C:\Users\LENOVO\Downloads\GUI\sobre.css">
    <script src="main.js"></script>
</head>
<body>
    <header>
    <h1>Guilherme Fernandes Neto</h1>

<h3>Gestão de Risco Agroclimático - FAO (Food And Agriculture Organization)</h3>
<h3>Eficiência Energética em Edifícios- UNIDO (United Nations Industrial Development Organization)</h3>
<p>Desenvolvo Projetos na Área de Energias Renováveis e Agricultura Sustentável e estou interessado em adentrar meu conhecimento ao mundo das máquinas e suas linguagens, afim de estreitar minha relação com a futura massa produtiva.</p>

<h3> Lista de disciplinas Relevantes </h3>
    <ol>
<li><a href="https://www.khanacademy.org/computing/computer-science/algorithms" target="_blanck">Algoritmos</a></li>

<li><a href="https://pt.khanacademy.org/computing/computer-programming/html-css?ref=resume_learning#intro-to-html" target="_blanck">
Introdução HTML e CSS</a></li>

</ol>

<h3>Filmes Recomendados </h3>
<ul>
<li>Modigliani - Paixão pela Vida</li> 
<figure id="filme-modigliane">
        <img src="http://br.web.img3.acsta.net/c_215_290/medias/nmedia/18/91/81/03/20164470.jpg" alt="Capa do filme Modigliani - Paixão pela vida">
        <figcaption> </figcaption> <!-- #isso é um comentário: pode colocar o nome, referencias...da imagem
        entre as tags figcaption -->
      </figure>

</ul>

</header>

<div id="rodape">
    <hr>
    <h4>Guilherme F. Neto - 2018</h4>
</body>
</html>